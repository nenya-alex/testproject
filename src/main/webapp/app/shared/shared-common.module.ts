import { NgModule } from '@angular/core';

import { DockerTestProjectSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [DockerTestProjectSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [DockerTestProjectSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class DockerTestProjectSharedCommonModule {}
