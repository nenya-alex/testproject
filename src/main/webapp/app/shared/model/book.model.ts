export interface IBook {
    id?: number;
    title?: string;
    editionYear?: number;
    price?: number;
    author?: string;
}

export class Book implements IBook {
    constructor(public id?: number, public title?: string, public editionYear?: number, public price?: number, public author?: string) {}
}
